import taichi as ti
import numpy as np


class MouseDataGen(object):
    def __init__(self, res):
        self.prev_mouse = None
        self.prev_id = 0
        self.res = res

    def __call__(self, gui):
        # [0:2]: normalized delta direction
        # [2:4]: current mouse xy
        # [4:5]: id
        mouse_data = np.zeros(5, dtype=np.float32)
        if gui.is_pressed(ti.GUI.LMB):
            # mxy: [x, y] in matrix coordinates i.e.:
            # y
            # ^
            # |
            # |-------> x
            mxy = np.array(gui.get_cursor_pos(), dtype=np.float32) * np.array(self.res)
            if self.prev_mouse is None:
                self.prev_mouse = mxy
                self.prev_id += 1
            else:
                mdir = mxy - self.prev_mouse
                mdir = mdir 
                mouse_data[0], mouse_data[1] = mdir[0], mdir[1]
                mouse_data[2], mouse_data[3] = mxy[0], mxy[1]
                mouse_data[4] = self.prev_id
                self.prev_mouse = mxy
        else:
            self.prev_mouse = None

        return np.array([mouse_data])