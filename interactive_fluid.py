import cv2
import queue

import time
import numpy as np
import taichi as ti

from mouse_data_gen import MouseDataGen
from test_data_gen import TestDataGen
from pose_data_gen import PoseDataGen
from mg_stable_fluid import step, resolution_checker, arts, res

MOUSE_INPUT = 0
MEDIA_PIPE = 1
ALPHA_POSE = 2
LIGHT_POSE = 3
TEST_INPUT = 4

# input_method = MEDIA_PIPE
# input_method = ALPHA_POSE
# input_method = MOUSE_INPUT
input_method = LIGHT_POSE
# input_method = TEST_INPUT

paused = False

use_interactive = False
use_fast_gui = True
frame_id = 0

show_skeleton = False


last_control_time = time.time()
is_idle = False
last_non_frozen_time = time.time()
is_frozen = False


if use_fast_gui:
    import fast_gui
    gui = fast_gui.GUI('Stable Fluid', res)
else:
    gui = ti.GUI('Stable Fluid', res)


cam_reader = None

if input_method in [MEDIA_PIPE,ALPHA_POSE,LIGHT_POSE]:
    from camera_reader import CameraReader
    cam_reader = CameraReader(res[0]/res[1], index=0)
    cam_reader.start()

pe = None
control_data_gen = None

if input_method == MOUSE_INPUT:
    control_data_gen = MouseDataGen(res=res)
elif input_method == TEST_INPUT:
    control_data_gen = TestDataGen(res=res,draw_type=1)
if input_method == MEDIA_PIPE:
    from pose_mediapipe import MediapipePoseEstimator
    pe = MediapipePoseEstimator(cam_reader )
elif input_method == ALPHA_POSE:
    from pose_alphapose import AlphaposePoseEstimator
    pe = AlphaposePoseEstimator(cam_reader )
elif input_method == LIGHT_POSE:
    from pose_light_openpose import LightOpenPoseEstimator
    pe = LightOpenPoseEstimator(cam_reader )
if pe is not None:
    pe.run_on_video(True)
    pe.start()
    control_data_gen = PoseDataGen(res=res, thres=0.1)



frame_id = 0
prev_pose_timestamp = None

while gui.running:
    if gui.get_event(ti.GUI.PRESS):
        e = gui.event
        if e.key == ti.GUI.ESCAPE:
            break
        elif e.key == 'd':
            visualize_d = True
            visualize_v = False
            visualize_c = False
        elif e.key == 'p':
            paused = not paused
        elif e.key == 'd':
            debug = not debug
        elif e.key == 'f':
            is_frozen = not is_frozen

    vis_frame = None
    control_data = []
    if not paused:
        using_guessed = False
        if input_method in [MEDIA_PIPE, ALPHA_POSE, LIGHT_POSE]:
            # if True or pe.current_timestamp > cam_reader.current_timestamp :
            keypoints, skeleton_frame = pe.current_result, pe.current_vis_frame
            if prev_pose_timestamp != pe.current_timestamp:
                control_data = control_data_gen(keypoints)
            else:
                using_guessed = True
                control_data = control_data_gen.guess()

            if show_skeleton:
                vis_frame = skeleton_frame
            else:
                vis_frame = cam_reader.current_frame

            prev_pose_timestamp = pe.current_timestamp
                
        elif input_method == MOUSE_INPUT:
            #vis_frame = cam_reader.current_frame
            control_data = control_data_gen(gui)
        elif input_method == TEST_INPUT:
            #vis_frame = cam_reader.current_frame
            control_data = control_data_gen()

        is_idle = False
        curr_time = time.time()

        if is_frozen:
            arts.is_frozen = True
            if curr_time - last_non_frozen_time >= 60:
                is_idle = True
        else:
            last_non_frozen_time = time.time()
            arts.is_frozen = False
        
        if len(control_data) == 0 or using_guessed:
            # print(curr_time - last_control_time, frame_id)
            if curr_time - last_control_time >= 60:
                is_idle = True
                # if frame_id % 600 != 0:
                    # is_idle = True
        else:
            last_control_time = time.time()
            if not is_frozen:
                is_idle = False
            
        if not is_idle:
            if pe is not None:
                pe.should_rest = False
            step(control_data)
            arts.is_idle = False
            # print("working")
        else:
            if pe is not None:
                pe.should_rest = True
            # TODO: only validate for logo_fluid2!
            arts.is_idle = True
            # print("idle", arts.is_idle)

   
    rendered_field = arts.visualize(vis_frame)
    gui.set_image(rendered_field)
    gui.show()
    frame_id += 1

cv2.destroyAllWindows()
