# References:
# http://developer.download.nvidia.com/books/HTML/gpugems/gpugems_ch38.html
# https://github.com/PavelDoGreat/WebGL-Fluid-Simulation
# https://www.bilibili.com/video/BV1ZK411H7Hc?p=4
# https://github.com/ShaneFX/GAMES201/tree/master/HW01

from ArtisticControl.color_fluid import ColorFluid
import time
import os
import numpy as np
import taichi as ti
import fast_gui
from mouse_data_gen import MouseDataGen
from test_data_gen import TestDataGen
from utils import TexPair
from ArtisticControl import Dye,ColorFluid,WarpMirror,LogoFluid,LogoFluid2


dim = 2
block_size = 16

# Full screen showcase
# res = (2560, 1440)  # (width (x), height (y))

# iPad Pro resolution dimension
#res = (1196, 834)  # (width (x), height (y))

#
res = (1920,1080)

def resolution_checker(res, block_size):
    # Padded the resolution so that can be divided by the block_size
    padded_res = []
    for r in res:
        assert r > block_size
        remainder = r % block_size
        if remainder != 0:
            r += block_size - remainder
        padded_res.append(r)
    print(f"Padded resolution: {padded_res}")
    return tuple(padded_res)


res = resolution_checker(res, block_size)

dt = 0.03
p_jacobi_iters = 500  # 40 for a quicker but less accurate result
p_mgpcg_iters = 5
p_mgjacobi_iters = 5


debug = False
paused = False
use_mgpcg = True
use_mg = True
use_mgjacobi = False

use_reflection = False
use_hacky_reflection = False

use_interactive = True
perf_use_gui = True
test_frame_num = 1000
use_fast_gui = True


frame_id = 0

ti.init(arch=ti.gpu, kernel_profiler=True, async_mode=False)

_velocities = ti.Vector.field(2, float, shape=res)
_new_velocities = ti.Vector.field(2, float, shape=res)
velocity_divs = ti.field(float, shape=res)
_pressures = ti.field(float, shape=res)
_new_pressures = ti.field(float, shape=res)



res_vec = ti.Vector(res)

mg_sovler = None
if use_mgpcg:
    from mgpcg import MGPCG
    mg_sovler = MGPCG(dim=2,
                  resolution=res,
                  block_size=block_size,
                  n_mg_levels=3,
                  use_multigrid=use_mg,
                  sparse=False
                      )
elif use_mgjacobi:
    from mg_jacobi import MGJacobi
    mg_sovler = MGJacobi(dim=2,
                      N=res,
                      block_size=block_size,
                      n_mg_levels=3,
                      down_scale=2)



velocities_pair = TexPair(_velocities, _new_velocities)
pressures_pair = TexPair(_pressures, _new_pressures)



@ti.func
def sample(qf, u, v):
    I = ti.Vector([int(u), int(v)])
    I = max(0, min(res_vec - 1, I))
    return qf[I]


@ti.func
def lerp(vl, vr, frac):
    # frac: [0.0, 1.0]
    return vl + frac * (vr - vl)


@ti.func
def bilerp(vf, p):
    u, v = p
    s, t = u - 0.5, v - 0.5
    # floor
    iu, iv = ti.floor(s), ti.floor(t)
    # fract
    fu, fv = s - iu, t - iv
    a = sample(vf, iu, iv)
    b = sample(vf, iu + 1, iv)
    c = sample(vf, iu, iv + 1)
    d = sample(vf, iu + 1, iv + 1)
    return lerp(lerp(a, b, fu), lerp(c, d, fu), fv)


# 3rd order Runge-Kutta
@ti.func
def backtrace(vf: ti.template(), p, dt: ti.template()):
    v1 = bilerp(vf, p)
    p1 = p - 0.5 * dt * v1
    v2 = bilerp(vf, p1)
    p2 = p - 0.75 * dt * v2
    v3 = bilerp(vf, p2)
    p -= dt * ((2 / 9) * v1 + (1 / 3) * v2 + (4 / 9) * v3)
    return p


@ti.kernel
def init_pressure_solver(v: ti.template()):
    for I in ti.grouped(v):
        mg_sovler.init_r(I, v[I])



@ti.func
def semi_lagrance(vf: ti.template(), qf: ti.template(),  x:ti.template(), dt:ti.template()):
    old_x = backtrace(vf, x, dt)
    q = bilerp(qf, old_x)
    return q

@ti.func
def maccormack(vf: ti.template(), qf: ti.template(),  x:ti.template(), dt:ti.template()):
    q = bilerp(qf, x)

    old_x = backtrace(vf, x, dt)
    old_q = bilerp(qf, old_x)

    old_new_x = backtrace(vf, old_x, -dt)
    old_new_q = bilerp(qf, old_new_x)

    err = q-old_new_q

    return old_q + err * 0.5

@ti.kernel
def advect(vf: ti.template(), qf: ti.template(), new_qf: ti.template(),dt:ti.template()):
    for I in ti.grouped(qf):
        new_qf[I] = maccormack(vf,qf,I+0.5,dt) 


@ti.kernel
def divergence(vf: ti.template()):
    for i, j in vf:
        vl = sample(vf, i - 1, j)
        vr = sample(vf, i + 1, j)
        vb = sample(vf, i, j - 1)
        vt = sample(vf, i, j + 1)
        vc = sample(vf, i, j)
        if i == 0:
            vl.x = -vc.x
        if i == res[0] - 1:
            vr.x = -vc.x
        if j == 0:
            vb.y = -vc.y
        if j == res[1] - 1:
            vt.y = -vc.y
        velocity_divs[i, j] = (vr.x - vl.x + vt.y - vb.y) * 0.5



@ti.kernel
def pressure_jacobi(pf: ti.template(), new_pf: ti.template()):
    for i, j in pf:
        pl = sample(pf, i - 1, j)
        pr = sample(pf, i + 1, j)
        pb = sample(pf, i, j - 1)
        pt = sample(pf, i, j + 1)
        div = velocity_divs[i, j]
        new_pf[i, j] = (pl + pr + pb + pt - div) * 0.25


@ti.kernel
def subtract_gradient(vf: ti.template(), pf: ti.template()):
    for i, j in vf:
        pl = sample(pf, i - 1, j)
        pr = sample(pf, i + 1, j)
        pb = sample(pf, i, j - 1)
        pt = sample(pf, i, j + 1)
        vf[i, j] -= 0.5 * ti.Vector([pr - pl, pt - pb])


#arts = Dye(res,advect)
#arts = ColorFluid(res,advect)
# arts = WarpMirror(res,advect)
curr_path = os.path.abspath(os.getcwd())
img_path= os.path.join(curr_path, "ArtisticControl/taichi_logo.png")
background_path = os.path.join(curr_path, "ArtisticControl/background1.jpg")

arts = LogoFluid2(res,advect,logo_path=img_path,background_path=background_path)


def run_advection(dt):
    advect(velocities_pair.cur, velocities_pair.cur, velocities_pair.nxt,dt)
    velocities_pair.swap()



def compute_pressure():
    divergence(velocities_pair.cur)
    if use_mgpcg:
        mg_sovler.init(velocity_divs, -1)
        mg_sovler.solve(max_iters=p_mgpcg_iters, verbose=False)
        mg_sovler.fetch_result(pressures_pair.cur)
    elif use_mgjacobi:
        mg_sovler.init(velocity_divs, 1)
        mg_sovler.solve(pressures_pair, max_iters=p_mgjacobi_iters, verbose=False)
    else:
        for it in range(p_jacobi_iters):
            pressure_jacobi(pressures_pair.cur, pressures_pair.nxt)
            pressures_pair.swap()



def step(control_data):
    global frame_id

    if not arts.initialized:
        arts.init()

    if not use_reflection and not use_hacky_reflection:
        arts.step(velocities_pair.cur,dt)
        
        run_advection(dt)
        
        arts.apply_control(control_data,velocities_pair.cur,dt)

        compute_pressure()    

        subtract_gradient(velocities_pair.cur, pressures_pair.cur)

    elif use_reflection:
        arts.step(velocities_pair.cur,dt)

        run_advection(dt * 0.5)
        
        arts.apply_control(control_data,velocities_pair.cur,dt)

        compute_pressure()    

        subtract_gradient(velocities_pair.cur, pressures_pair.cur)
        subtract_gradient(velocities_pair.cur, pressures_pair.cur)

        run_advection(dt * 0.5)

        compute_pressure()    

        subtract_gradient(velocities_pair.cur, pressures_pair.cur)

    elif use_hacky_reflection:
        if frame_id % 2 == 0:

            arts.step(velocities_pair.cur,dt)
            run_advection(dt)

            arts.apply_control(control_data,velocities_pair.cur,dt)

            compute_pressure()    

            subtract_gradient(velocities_pair.cur, pressures_pair.cur)
        else:

            subtract_gradient(velocities_pair.cur, pressures_pair.cur)

            arts.step(velocities_pair.cur,dt)

            run_advection(dt )

            arts.apply_control(control_data,velocities_pair.cur,dt)

            compute_pressure()    

            subtract_gradient(velocities_pair.cur, pressures_pair.cur)

    
    arts.finalize_step(velocities_pair.cur)
    frame_id += 1

