import cv2
from utils import current_milli_time
import threading


class CameraReader(threading.Thread):
    def __init__(self, aspect_ratio, index=0):
        threading.Thread.__init__(self)
        self.cam = cv2.VideoCapture(index)
        if not self.cam.isOpened():
            print("Cannot open camera")
            exit()
        suceess, self.current_frame = self.cam.read()
        self.current_timestamp = current_milli_time()
        self.aspect_ratio = aspect_ratio
    
    def frame_from_video(self):
        success, frame = self.cam.read()
        h,w, _ = frame.shape
        if w / h > self.aspect_ratio:
            pad = int((w - h * self.aspect_ratio) / 2)
            frame = frame[:,pad:w-pad, :]
        elif w / h < self.aspect_ratio:
            pad = int((h - w / self.aspect_ratio) / 2)
            frame = frame[ pad:h-pad, :,:]
        
        if success:
            frame = cv2.rotate(frame, cv2.cv2.ROTATE_90_COUNTERCLOCKWISE)
            frame = cv2.flip(frame, 1)
            #print(frame.shape)
            return frame
        else:
            return None

    def run(self):
        while True:
            frame = self.frame_from_video()
            self.current_frame = frame
            self.current_timestamp = current_milli_time()
