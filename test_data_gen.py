import numpy as np

class TestDataGen:
    class _DrawDriver:
        def __init__(self, path_click, path_drag):
            self.index = 0
            self.path_click = path_click
            self.path_drag = path_drag
            assert len(path_click) == len(path_drag)
            self.path_len = len(path_click)
            self.res = None

        def draw(self, counter):
            if counter % 2 == 0:
                self.res = self.path_click[self.index % self.path_len]
            else:
                self.res = self.path_drag[self.index % self.path_len]
            self.index += 1
            return self.res

    def __init__(self, res, draw_type=0):
        self.prev_mouse = None
        self.prev_id = 0
        self.draw_type = draw_type
        self.res = res
        self.radius_initial = 0.618 / 2
        self.period_initial = 50
        self.angular_v_initial = 360 / self.period_initial

        # Origin position
        self.x_offset = 0.5
        self.y_offset = 0.5
        self.theta_offset = 90

        # Initial state
        self.theta = 90
        self.x = self.x_offset
        self.y = self.y_offset + self.radius_initial
        self.radius = self.radius_initial
        self.counter = 0
        self.angular_v = self.angular_v_initial
        self.period = self.period_initial

        self.x_origin = self.x_offset
        self.y_origin = self.y_offset

        # Draw circle
        self.draw_circle_driver = TestDataGen._DrawDriver(*self.draw_circle())
        # Draw taichi
        self.draw_taichi_driver = TestDataGen._DrawDriver(*self.draw_taichi())

    def update_states(self, angular_v):
        self.theta += angular_v
        self.theta = self.theta % 360
        self.x = self.x_origin + self.radius * np.cos(self.theta/180 * np.pi)
        self.y = self.y_origin + self.radius * np.sin(self.theta/180 * np.pi)

    def reset(self):
        self.theta = self.theta_offset
        self.x = self.x_offset
        self.y = self.y_offset
        self.radius = self.radius_initial
        self.x_origin = self.x_offset
        self.y_origin = self.y_offset
        self.period = self.period_initial

    def draw_circle(self):
        path_click = []
        path_drag = []
        for i in range(int(self.period)):
            self.update_states(self.angular_v)
            path_click.append([self.x, self.y])
            self.update_states(1)
            path_drag.append([self.x, self.y])
            self.update_states(-1)
        self.reset()
        return path_click, path_drag

    def draw_taichi(self):
        path_click, path_drag = self.draw_circle()

        self.radius = self.radius_initial / 2
        self.y += self.radius
        self.y_origin += self.radius
        u_path_click, u_path_drag = self.draw_circle()

        self.radius = self.radius_initial / 2
        self.y -= self.radius
        self.y_origin -= self.radius
        l_path_click, l_path_drag = self.draw_circle()

        path_click.extend(u_path_click[:len(u_path_click) // 2])
        path_click.extend(l_path_click[len(l_path_click) // 2:][::-1])

        path_drag.extend(u_path_drag[:len(u_path_drag) // 2])
        path_drag.extend(l_path_drag[len(l_path_drag) // 2:][::-1])

        # Draw upper eye
        self.y += self.radius_initial / 2
        self.y_origin += self.radius_initial / 2
        self.radius = self.radius_initial / 20
        self.period = 2
        ue_path_click, ue_path_drag = self.draw_circle()

        # Draw lower eye
        self.y -= self.radius_initial / 2
        self.y_origin -= self.radius_initial / 2
        self.radius = self.radius_initial / 20
        self.period = 2
        le_path_click, le_path_drag = self.draw_circle()

        path_click.extend(ue_path_click)
        path_click.extend(le_path_click)

        path_drag.extend(ue_path_drag)
        path_drag.extend(le_path_drag)

        return path_click, path_drag

    def __call__(self):
        # [0:2]: normalized delta direction
        # [2:4]: current mouse xy
        # [4]: id
        test_data = np.zeros(8, dtype=np.float32)

        x, y = 0, 0
        if self.draw_type == 0:
            x, y = self.draw_taichi_driver.draw(self.counter)
        elif self.draw_type == 1:
            x, y = self.draw_circle_driver.draw(self.counter)
        else:
            raise NotImplementedError

        mxy = np.array([x, y], dtype=np.float32) * np.array(self.res)
        if self.prev_mouse is None:
            self.prev_mouse = mxy
            self.prev_id += 1
        else:
            mdir = mxy - self.prev_mouse
            mdir = mdir 
            test_data[0], test_data[1] = mdir[0], mdir[1]
            test_data[2], test_data[3] = mxy[0], mxy[1]
            test_data[4] = self.prev_id
            self.prev_mouse = mxy
        self.counter += 1
        return np.array([test_data])
