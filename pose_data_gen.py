import numpy as np

from collections import defaultdict


class FluidSource:
    def __init__(self,
                 source_id=None,
                 prev_xy=None,
                 prev_dir=None):
        self.source_id = source_id
        self.prev_xy = prev_xy
        self.prev_dir = prev_dir


class PoseDataGen(object):
    num_kpts = 18
    kpt_names = ['nose', 'neck',
                 'r_sho', 'r_elb', 'r_wri', 'l_sho', 'l_elb', 'l_wri',
                 'r_hip', 'r_knee', 'r_ank', 'l_hip', 'l_knee', 'l_ank',
                 'r_eye', 'l_eye',
                 'r_ear', 'l_ear']

    def __init__(self, res, thres=0.5):
        self.prev_fluid_sources = dict()
        self.res = res
        self.thres = thres
        self.draw_selected = [18, 19]
        self.prev_source_id = 0

        self.prev_max_displacement = np.linalg.norm(res)*0.8
        self.dir_thres = 0.2 * 1000 # restrict the vector norm to handle keypoints junmping

        self.arm_length_diff_thres = 1.5 * 1000
        self.prev_left_arm_length = 0.0
        self.prev_right_arm_length = 0.0

    def _violation_checker(self, kp):
        return kp['x'] < 0 or kp['x'] > 1.0 or kp['y'] < 0 or kp['y'] > 1.0 # or kp['visibility'] < self.thres

    def _distance(self, kp1, kp2):
        return np.linalg.norm(np.array([(kp1['x'] - kp2['x'])*self.res[0],
                                        (kp1['y'] - kp2['y'])*self.res[1]]))

    def __call__(self, kp_set):
        # [0:2]: normalized delta direction
        # [2:4]: current mouse xy
        # [4:5]: id

        # COCO skeleton convention
        #       14  15
        #    16 / \/ \ 17
        #          | 0
        #   2  ----1----  5
        #   3  |   |    | 6
        #   4  |   |    | 7
        #         / \
        #     8  /   \ 11
        #       |     |
        #      9|     | 12
        #       |     |
        #     10|     | 13

        SKIP = False
        if len(kp_set) == 0:
            pose_data = []
            return pose_data
        pose_data_all = []
        current_person_ids = set()
        for person_id, kp_one_person in kp_set.items():
            current_person_ids.add(person_id)
            kp_list = [kp_one_person[x] for x in self.draw_selected]
            if person_id not in self.prev_fluid_sources:
                source_id = self.prev_source_id + 1
                self.prev_source_id = source_id
                fluid_source = FluidSource(prev_xy=[None]*len(self.draw_selected),
                                           prev_dir=[None]*len(self.draw_selected),
                                           source_id=source_id)
                self.prev_fluid_sources[person_id] = fluid_source

            curr_left_arm_length = 0.0
            curr_right_arm_length = 0.0
            # Compute current left/right arm length
            if not (self._violation_checker(kp_one_person[3]) or self._violation_checker(kp_one_person[4])):
                curr_right_arm_length = self._distance(kp_one_person[3], kp_one_person[4])
            if not (self._violation_checker(kp_one_person[6]) or self._violation_checker(kp_one_person[7])):
                curr_left_arm_length = self._distance(kp_one_person[6], kp_one_person[7])
            self.prev_max_displacement = max(self.prev_max_displacement, (curr_left_arm_length + curr_right_arm_length) * 0.5)

            for idx, kp in enumerate(kp_list):
                if self._violation_checker(kp):
                    self.prev_fluid_sources[person_id].prev_xy[idx] = None
                    self.prev_fluid_sources[person_id].prev_dir[idx] = None
                    continue
                #print(idx,curr_left_arm_length,curr_right_arm_length,self.prev_left_arm_length,self.prev_right_arm_length)
                if idx == 1:
                    if curr_left_arm_length != 0 and self.prev_left_arm_length != 0:
                        bigger_len = max(curr_left_arm_length,self.prev_left_arm_length)
                        smaller_len = min(curr_left_arm_length,self.prev_left_arm_length)
                        #print(bigger_len,smaller_len,"    ",bigger_len/smaller_len)
                        if bigger_len / smaller_len > self.arm_length_diff_thres:
                            print("skipping")
                            self.prev_fluid_sources[person_id].prev_xy[idx] = None
                            self.prev_fluid_sources[person_id].prev_dir[idx] = None
                            continue

                if idx == 0:
                    if curr_right_arm_length != 0 and self.prev_right_arm_length != 0:
                        bigger_len = max(curr_right_arm_length,self.prev_right_arm_length)
                        smaller_len = min(curr_right_arm_length,self.prev_right_arm_length)
                        #print(bigger_len,smaller_len,"    ",bigger_len/smaller_len)
                        if bigger_len / smaller_len > self.arm_length_diff_thres:
                            print("skipping")
                            self.prev_fluid_sources[person_id].prev_xy[idx] = None
                            self.prev_fluid_sources[person_id].prev_dir[idx] = None
                            continue

                pose_data = np.zeros(5, dtype=np.float32)
                # if idx == 1:
                #     print(idx, '   ', kp['visibility'])
                if kp['visibility'] > self.thres:
                    mxy = np.array([kp['x'], kp['y']], dtype=np.float32) * np.array(self.res)
                    prev_pos = self.prev_fluid_sources[person_id].prev_xy[idx]

                    if prev_pos is not None:
                        mdir = mxy - prev_pos
                        #mdir = mdir / (np.linalg.norm(mdir) + 1e-5)
                        self.prev_fluid_sources[person_id].prev_xy[idx] = mxy
                        self.prev_fluid_sources[person_id].prev_dir[idx] = mdir
                        
                        # Skip the keypoints with large displacement
                        if np.linalg.norm(mdir) > self.dir_thres * self.prev_max_displacement:
                            SKIP = True
                            self.prev_fluid_sources[person_id].prev_xy[idx] = None
                            self.prev_fluid_sources[person_id].prev_dir[idx] = None
                            print(f"skipped, current displacement: {np.linalg.norm(mdir)}, max disp. allowed: {self.dir_thres * self.prev_max_displacement}")
                            continue

                        pose_data[0], pose_data[1] = mdir[0], mdir[1]
                        pose_data[2], pose_data[3] = mxy[0], mxy[1]
                        pose_data[4] = self.prev_fluid_sources[person_id].source_id
                        pose_data_all.append(pose_data)
                    else:
                        self.prev_fluid_sources[person_id].prev_xy[idx] = mxy
            self.prev_left_arm_length = curr_left_arm_length
            self.prev_right_arm_length = curr_right_arm_length
        id_to_remove = []
        for id in self.prev_fluid_sources:
            if id not in current_person_ids:
                id_to_remove.append(id)
        for id in id_to_remove:
            del self.prev_fluid_sources[id]
        if SKIP:
            print('draw pose', len(pose_data_all))
        return pose_data_all

    # not used right now
    def guess(self):
        pose_data_all = []
        SKIP = False

        for person_id in self.prev_fluid_sources:
            source = self.prev_fluid_sources[person_id]
            for kp_id in range(len(source.prev_xy)):
                pose_data = np.zeros(8, dtype=np.float32)

                old_xy = source.prev_xy[kp_id]
                old_dir = source.prev_dir[kp_id]
                old_source_id = source.source_id

                if old_xy is None or old_dir is None:
                    continue

                old_dir_norm = (np.linalg.norm(old_dir)+1e-5)
                # Skip the keypoints with large displacementgit 
                if old_dir_norm > self.dir_thres * self.prev_max_displacement:
                    SKIP = True
                    self.prev_fluid_sources[person_id].prev_xy[kp_id] = None
                    self.prev_fluid_sources[person_id].prev_dir[kp_id] = None
                    print(
                        f"Guess skipped, current displacement: {old_dir_norm}, max disp. allowed: {self.dir_thres * self.prev_max_displacement}")
                    continue
                # because we are guessing, we don't wanna move too far.
                guessed_dir = old_dir / old_dir_norm
                guessed_xy = old_xy + guessed_dir

                pose_data[0], pose_data[1] = guessed_dir[0] * old_dir_norm * 0.5, guessed_dir[1]*old_dir_norm * 0.5
                pose_data[2], pose_data[3] = guessed_xy[0], guessed_xy[1]
                pose_data[4] = old_source_id

                source.prev_xy[kp_id] = guessed_xy
                source.prev_dir[kp_id] = guessed_dir
                pose_data_all.append(pose_data)
        if SKIP:
            print('draw guess pose', len(pose_data_all))
        return np.array(pose_data_all)
