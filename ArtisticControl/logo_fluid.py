import cv2
import os
import taichi as ti
import numpy as np
from utils import TexPair


@ti.func
def squared_distance_between_point_and_line_segment(p:ti.template(),x0:ti.template(),x1:ti.template()):
    r = (x1-x0)
    result = 0.0
    if r[0]==0 and r[1] == 0:
        result = (p-x0).norm_sqr()
    tmax = r.norm()
    r = r / tmax
    t = (p-x0).dot(r)
    if t < 0:
        result = (p-x0).norm_sqr()
    elif t > tmax:
        result = (p-x1).norm_sqr()
    # comment out the following two lines for some magical effects.
    else:
        result = (x0 + t*r - p).norm_sqr()
    return result





@ti.data_oriented
class LogoFluid:
    def __init__(self,res,advect_kernel,img_path) -> None:
        self.res = res

        self.dye_decay = 0.99
        self.f_strength = 10000.0
        self.force_radius = np.mean(res) / 5.0
        self.dye_buffer = ti.Vector.field(3, float, shape=res)
        self.new_dye_buffer = ti.Vector.field(3, float, shape=res)
        self.has_dye = ti.field(int,shape = ())
        self.dyes_pair = TexPair(self.dye_buffer, self.new_dye_buffer)
        self.interactive_render = ti.Vector.field(3, float, shape=res)
        self.advect_kernel = advect_kernel
        self.source_on_line = True

        self.initialized = False

        self.last_camera_frame = None
        self.frame_id = 0
        self.img_path = img_path
        self.img = cv2.imread(self.img_path) / 255.0
        self.img = cv2.transpose(self.img)
        self.img = cv2.flip(self.img, flipCode=1)

        print("original shape", self.img.shape)
        self.pad_image()

        self.mask = ti.field(int, shape=res)


    def pad_image(self):
        res_w, res_h = self.res
        img_w, img_h, _ = self.img.shape
        ap_res = res_w / res_h
        ap_img = img_w / img_h
        ap_diff = ap_res - ap_img

        if ap_diff > 0:
            pad = int((img_h * ap_res - img_w) // 2)
            self.img = cv2.copyMakeBorder(
                self.img,
                top=pad,
                bottom=pad,
                left=0,
                right=0,
                borderType=cv2.BORDER_CONSTANT,
                value=(1.0,1.0,1.0)
            )
        else:
            pad = int((img_w / ap_res - img_h) // 2)
            self.img = cv2.copyMakeBorder(
                self.img,
                top=0,
                bottom=0,
                left=pad,
                right=pad,
                borderType=cv2.BORDER_CONSTANT,
                value=(1.0,1.0,1.0)
            )
        print("after padding", self.img.shape)
        # assert self.img.shape[0] / self.img.shape[1] == res_h
        self.img = cv2.resize(self.img, (res_h, res_w))


    @ti.kernel
    def init_dye(self,dye: ti.template(), logo:ti.ext_arr()):
        # for i,j in dye:
        #     dye[i,j][0] = ti.sin(i/200) ** 2
        #     dye[i,j][1] = ti.sin(j/200) ** 2
        #     dye[i,j][2] = 0.5 * (1.0 - dye[i,j][0] + 1.0 - dye[i,j][1])

        for i,j in dye:
            if logo[i, j, 0] > 0.5 or logo[i, j, 1] > 0.5 or logo[i, j, 2] > 0.5:
                self.mask[i, j] = 1
            else:
                self.mask[i, j] = 0
            # dye[i,j][0] = ti.sin(i/200) ** 2
            # dye[i,j][1] = ti.sin(j/200) ** 2
            # dye[i,j][2] = 0.5 * (1.0 - dye[i,j][0] + 1.0 - dye[i,j][1])
            dye[i,j][0] = logo[i, j, 0]
            dye[i,j][1] = logo[i, j, 1]
            dye[i,j][2] = logo[i, j, 2]

    def init(self):
        print('logo shape', self.img.shape)
        self.init_dye(self.dyes_pair.cur, self.img)
        self.initialized = True

    @ti.kernel
    def apply_impulse(self,vf: ti.template(), dyef: ti.template(),
                    imp_data: ti.ext_arr(),dt:ti.template()):

        for i, j in vf:
            dir = ti.Vector([imp_data[0], imp_data[1]])
            dir_norm =  dir.norm()+1e-5
            factor = 0.0
            distance_sqr = 0.0

            if not self.source_on_line:
                endpoint = ti.Vector([imp_data[2], imp_data[3]])
                cell_point = ti.Vector([i+0.5, j+0.5])
                distance_sqr = (cell_point-endpoint).norm_sqr()
                
            else:
                endpoint = ti.Vector([imp_data[2], imp_data[3]])
                dir = ti.Vector([imp_data[0], imp_data[1]])
                startpoint = endpoint - dir
                cell_point = ti.Vector([i+0.5, j+0.5])
                distance_sqr = squared_distance_between_point_and_line_segment(cell_point,startpoint,endpoint)

            factor = ti.exp(-distance_sqr / self.force_radius) 

            momentum = (dir / dir_norm * self.f_strength * factor ) * dt
            v = vf[i, j]
            vf[i, j] = v + momentum


    def apply_control(self,control_data,velocity_feild,dt):
        for data in control_data:
            self.apply_impulse(velocity_feild, self.dyes_pair.cur, data,dt)
        

    @ti.kernel
    def source_color(self,dye:ti.template(),velocity:ti.template(),vis_frame:ti.ext_arr(),w:int,h:int,frame_id:int):
        for i, j in dye:
            i_v = int(i * w / self.res[0])
            j_v = int(j * h / self.res[1])
            image = ti.Vector([0.0,0.0,0.0])
            image[0] = min(255,vis_frame[i_v,j_v,2]) / 255
            image[1] = min(255,vis_frame[i_v,j_v,1]) / 255
            image[2] = min(255,vis_frame[i_v,j_v,0]) / 255
            c = ti.Vector([0.0, 0.0, 0.0])
            if self.mask[i, j] > 0:
                c[0] = ti.sin(i/200) ** 2 * 0.75 + ti.sin(frame_id/200) ** 2 * 0.25
                c[1] = ti.sin(j/200) ** 2 * 0.75 + ti.cos(frame_id/200) ** 2 * 0.25
                c[2] = 0.5 * (1.0 - c[0] + 1.0 - c[1])

            else:
                c[0] = 0.0
                c[1] = 0.0
                c[2] = 0.0

            # c[0] = ti.sin((i+frame_id)/200) ** 2
            # c[1] = ti.sin((j+frame_id)/200) ** 2
            # c[2] = 0.5 * (1.0 - c[0] + 1.0 - c[1])

            # c = ti.Vector([0.0,0.0,0.0])
            # c[0] = ti.sin(i/200) ** 2 * 0.75 + ti.sin(frame_id/200) ** 2 * 0.25
            # c[1] = ti.sin(j/200) ** 2 * 0.75 + ti.cos(frame_id/200) ** 2 * 0.25
            # c[2] = 0.5 * (1.0 - c[0] + 1.0 - c[1])

            alpha = min(0.005 * velocity[i,j].norm() / 4000)
            alpha = 0.05

            dye[i, j] = dye[i, j] * (1-alpha) + c * alpha 
        
    def step(self,velocity_feild,dt):
        if self.last_camera_frame is not None:
            w,h,_ = self.last_camera_frame.shape
            self.source_color(self.dyes_pair.cur,velocity_feild, self.last_camera_frame,w,h,self.frame_id)

        self.advect_kernel(velocity_feild, self.dyes_pair.cur, self.dyes_pair.nxt,dt)
        self.dyes_pair.swap()

    def finalize_step(self,velocity_feild):
        pass

    @ti.kernel
    def blend_interaction(self,image: ti.template(), dye: ti.template(), vis_frame:ti.ext_arr(),w:int,h:int):
        # overlay fluids on camera frame
        for i, j in dye:
            alpha =  0.5
            i_v = int(i * w / self.res[0])
            j_v = int(j * h / self.res[1])
            image[i, j][0] = min(255,vis_frame[i_v,j_v,2]) / 255
            image[i, j][1] = min(255,vis_frame[i_v,j_v,1]) / 255
            image[i, j][2] = min(255,vis_frame[i_v,j_v,0]) / 255

            image[i, j] = image[i, j] *  0.4 + dye[i, j] * 1


    def visualize(self,camera_frame):
        self.frame_id += 1
        if camera_frame is not None:
            w,h,_ = camera_frame.shape
            self.blend_interaction(self.interactive_render, self.dyes_pair.cur,camera_frame,w,h)
            self.last_camera_frame = camera_frame
            return self.interactive_render
        else:
            return self.dyes_pair.cur


if __name__ == "__main__":
    curr_path = os.path.abspath(os.getcwd())
    img_path= os.path.join(curr_path, "ArtisticControl/taichi_logo.png")
    print(np.unique(cv2.imread(img_path)))
    lf = LogoFluid(res=(512, 1024), advect_kernel=None, img_path=img_path)
