import taichi as ti
import numpy as np
from utils import TexPair


@ti.func
def squared_distance_between_point_and_line_segment(p:ti.template(),x0:ti.template(),x1:ti.template()):
    r = (x1-x0)
    result = 0.0
    if r[0]==0 and r[1] == 0:
        result = (p-x0).norm_sqr()
    tmax = r.norm()
    r = r / tmax
    t = (p-x0).dot(r)
    if t < 0:
        result = (p-x0).norm_sqr()
    elif t > tmax:
        result = (p-x1).norm_sqr()
    # comment out the following two lines for some magical effects.
    else:
        result = (x0 + t*r - p).norm_sqr()
    return result





@ti.data_oriented
class Dye:
    def __init__(self, res, advect_kernel) -> None:
        self.res = res

        self.dye_decay = 0.99
        self.f_strength = 10000.0
        self.force_radius = np.mean(res) / 5.0
        self.dye_buffer = ti.Vector.field(3, float, shape=res)
        self.new_dye_buffer = ti.Vector.field(3, float, shape=res)
        self.has_dye = ti.field(int,shape = ())
        self.dyes_pair = TexPair(self.dye_buffer, self.new_dye_buffer)
        self.interactive_render = ti.Vector.field(3, float, shape=res)
        self.advect_kernel = advect_kernel
        self.source_on_line = True

        self.source_colors = dict()
        self.initialized = False

    def init(self):
        self.initialized = True

    @ti.kernel
    def apply_impulse(self, vf: ti.template(), dyef: ti.template(),
                    imp_data: ti.ext_arr(), dt: ti.template(), mean_res: float):

        for i, j in vf:
            dir = ti.Vector([imp_data[0], imp_data[1]])
            dir_norm = dir.norm()+1e-5
            factor = 0.0
            distance_sqr = 0.0

            if not self.source_on_line:
                endpoint = ti.Vector([imp_data[2], imp_data[3]])
                cell_point = ti.Vector([i+0.5, j+0.5])
                distance_sqr = (cell_point-endpoint).norm_sqr()
                
            else:
                endpoint = ti.Vector([imp_data[2], imp_data[3]])
                dir = ti.Vector([imp_data[0], imp_data[1]])
                startpoint = endpoint - dir
                cell_point = ti.Vector([i+0.5, j+0.5])
                distance_sqr = squared_distance_between_point_and_line_segment(cell_point, startpoint, endpoint)

            factor = ti.exp(-distance_sqr / self.force_radius) 

            momentum = (dir / dir_norm * self.f_strength * factor) * dt
            v = vf[i, j]
            vf[i, j] = v + momentum

            # add dye
            max_dye = 1
            dye_color = dyef[i, j]
            if dir.norm() > 0.5:
                dye_color += ti.exp(-distance_sqr * (4 / (mean_res / 15)**2)) * dir_norm / (mean_res/15.0) * ti.Vector(
                    [imp_data[4], imp_data[5], imp_data[6]])
                dye_color[0] = min(dye_color[0], max_dye)
                dye_color[1] = min(dye_color[1], max_dye)
                dye_color[2] = min(dye_color[2], max_dye)
            dyef[i, j] = dye_color

    @ti.kernel
    def apply_dye_decay(self, dyef: ti.template()):
        for i,j in dyef:
            dyef[i,j] = dyef[i,j] * self.dye_decay

    @ti.kernel
    def check_dye(self,dye:ti.template()):
        self.has_dye[None] = 0
        for i,j in dye:
            threshold = 0.01
            if dye[i,j][0] > threshold or dye[i,j][1] > threshold or dye[i,j][2] > threshold:
                self.has_dye[None] = 1


    def apply_control(self,control_data,velocity_feild,dt):
        for data in control_data:
            source_id = data[4]
            if source_id not in self.source_colors:
                self.source_colors[source_id] = (np.random.rand(3) * 0.7) + 0.3
                self.source_colors[source_id] = np.array(self.source_colors[source_id], dtype=np.float32)
            impulse_data = data[:4]
            color = self.source_colors[source_id]
            self.apply_impulse(velocity_feild, self.dyes_pair.cur, np.concatenate([impulse_data, color]), dt, np.mean(self.res))
        ids_to_remove = []
        for source_id in self.source_colors:
            found = False
            for data in control_data:
                if data[4] == source_id:
                    found = True
            if not found:
                ids_to_remove.append(source_id)
        for id in ids_to_remove:
            del self.source_colors[id]

        
    def step(self,velocity_feild,dt):
        self.advect_kernel(velocity_feild, self.dyes_pair.cur, self.dyes_pair.nxt,dt)
        self.dyes_pair.swap()
        self.apply_dye_decay(self.dyes_pair.cur)

    def finalize_step(self,velocity_feild):
        self.check_dye(self.dyes_pair.cur)
        if self.has_dye[None] == 0:
            velocity_feild.fill(0)
            self.dyes_pair.cur.fill(0)


    @ti.kernel
    def blend_interaction(self,image: ti.template(), dye: ti.template(), vis_frame:ti.ext_arr(),w:int,h:int):
        # overlay fluids on camera frame
        for i, j in dye:
            alpha = dye[i, j].norm()
            i_v = int(i * w / self.res[0])
            j_v = int(j * h / self.res[1])
            image[i, j][0] = min(255,vis_frame[i_v,j_v,2]) / 255
            image[i, j][1] = min(255,vis_frame[i_v,j_v,1]) / 255
            image[i, j][2] = min(255,vis_frame[i_v,j_v,0]) / 255

            image[i, j] = image[i, j] * 0.3 * (1-alpha) + dye[i, j] * alpha # * 0.2


    def visualize(self,camera_frame):
        if camera_frame is not None:
            w,h,_ = camera_frame.shape
            self.blend_interaction(self.interactive_render, self.dyes_pair.cur,camera_frame,w,h)
            return self.interactive_render
        else:
            return self.dyes_pair.cur