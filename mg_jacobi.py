import numpy as np
import taichi as ti


@ti.data_oriented
class MGJacobi:
    def __init__(self,
                 dim=2,
                 N=512,
                 offset=None,
                 n_mg_levels=5,
                 block_size=8,
                 down_scale=4,
                 real=ti.f32,
                 sparse=False):
        self.N = N
        self.n_mg_levels = n_mg_levels
        self.pre_and_post_smoothing = 2
        self.bottom_smoothing = 10
        self.down_scale = down_scale
        self.dim = dim
        self.real = real

        if offset is None:
            self.offset = (-self.N // 2, ) * self.dim
        else:
            self.offset = offset
            assert len(offset) == self.dim

        self.r = [ti.field(dtype=self.real) for _ in range(self.n_mg_levels)]
        self.z = [ti.field(dtype=self.real) for _ in range(self.n_mg_levels)]
        self.z_new = [ti.field(dtype=self.real) for _ in range(self.n_mg_levels)]

        # self.x = ti.field(dtype=self.real)
        indices = ti.ijk if self.dim == 3 else ti.ij

        coarsened_offset = self.offset
        coarsened_grid_size = self.N
        self.grids = []

        for l in range(self.n_mg_levels):
            sparse_grid_size = coarsened_grid_size + block_size * 2
            sparse_grid_offset = [o - block_size for o in coarsened_offset]
            print(f'Level {l}')
            print(f'  coarsened_grid_size {coarsened_grid_size}')
            print(f'  coarsened_offset {coarsened_offset}')
            print()
            print(f'  sparse_grid_size {sparse_grid_size}')
            print(f'  sparse_offset {sparse_grid_offset}')

            grid = None
            if sparse:
                grid = ti.root.pointer(indices, sparse_grid_size // block_size)
            else:
                grid = ti.root.pointer(indices, coarsened_grid_size // block_size)

            fields = []

            # if l == 0:
            #     fields += [self.x]
            fields += [self.r[l], self.z[l], self.z_new[l]]

            if sparse:
                for f in fields:
                    grid.dense(indices,
                               block_size).place(f, offset=sparse_grid_offset)
            else:
                for f in fields:
                    grid.dense(indices, block_size).place(f)

            self.grids.append(grid)

            new_coarsened_offset = []
            for o in coarsened_offset:
                new_coarsened_offset.append(o // self.down_scale)
            coarsened_offset = new_coarsened_offset
            coarsened_grid_size //= self.down_scale

    def clear(self):
        # TODO: deactivate_all here doesn't seem to work for some unknown reason...
        # for g in self.grids:
        #     g.deactivate_all()
        self.x.fill(0)

    @ti.func
    def init_r(self, I, r_I):
        self.r[0][I] = r_I
        self.z[0][I] = 0
        self.z_new[0][I] = 0

    @ti.kernel
    def init(self, r: ti.template(), k: ti.template()):
        for I in ti.grouped(r):
            self.init_r(I, r[I] * k)

    @ti.kernel
    def fetch_result(self, x: ti.template()):
        for I in ti.grouped(x):
            x[I] = self.x[I]

    @ti.func
    def neighbor_sum(self, x, I):
        ret = ti.cast(0.0, self.real)
        for i in ti.static(range(self.dim)):
            offset = ti.Vector.unit(self.dim, i)
            ret += x[I + offset] + x[I - offset]
        return ret

    @ti.kernel
    def reduce(self, p: ti.template(), q: ti.template()):
        self.sum[None] = 0
        for I in ti.grouped(p):
            self.sum[None] += p[I] * q[I]

    @ti.kernel
    def restrict(self, l: ti.template()):
        # ti.block_local(self.z[l])
        for I in ti.grouped(self.r[l]):
            residual = (self.neighbor_sum(self.z[l], I) - 2 * self.dim * self.z[l][I]
                                       ) - self.r[l][I]
            self.r[l + 1][I // self.down_scale] += residual * 1.0 / self.down_scale  # 0.5

    @ti.kernel
    def prolongate(self, l: ti.template()):
        for I in ti.grouped(self.z[l]):
            self.z[l][I] -= self.z[l + 1][I // self.down_scale]

    @ti.kernel
    def smooth(self, l: ti.template(), phase: ti.template()):
        # phase = red-black Gauss-Seidel phase
        for I in ti.grouped(self.r[l]):
            if (I.sum()) & 1 == phase:
                self.z[l][I] = (self.r[l][I] + self.neighbor_sum(
                    self.z[l], I)) / (2 * self.dim)

    @ti.func
    def sample(self, qf, u, v, n):
        I = ti.Vector([int(u), int(v)])
        I = max(0, min(n - 1, I))
        return qf[I]

    @ti.kernel
    def jacobi_smooth(self, l: ti.template(),  pf: ti.template(), new_pf: ti.template(), res: ti.template()):
        for i, j in pf:
            pl = self.sample(pf, i - 1, j, res)
            pr = self.sample(pf, i + 1, j, res)
            pb = self.sample(pf, i, j - 1, res)
            pt = self.sample(pf, i, j + 1, res)
            div = self.r[l][i, j]
            new_pf[i, j] = (pl + pr + pb + pt - div) * 0.25

    def solve(self,
              pressures_pair,
              max_iters=-1,
              verbose=False):

        residuals = []
        iter = 0
        self.z[0] = pressures_pair.cur
        self.z_new[0] = pressures_pair.nxt

        while max_iters == -1 or iter < max_iters:
            for l in range(self.n_mg_levels - 1):
                res_cur = self.N // (2 ** l)
                for i in range(self.pre_and_post_smoothing << l):
                    self.jacobi_smooth(l, self.z[l], self.z_new[l], res_cur)
                    self.z[l], self.z_new[l] = self.z_new[l], self.z[l]
                self.z[l + 1].fill(0)
                self.r[l + 1].fill(0)
                self.restrict(l)

            bottom = self.n_mg_levels - 1
            for i in range(self.bottom_smoothing):
                self.jacobi_smooth(bottom, self.z[bottom], self.z_new[bottom], self.N // (2**l))
                self.z[bottom], self.z_new[bottom] = self.z_new[bottom], self.z[bottom]

            for l in reversed(range(self.n_mg_levels - 1)):
                self.prolongate(l)
                res_cur = self.N // (2 ** l)
                for i in range(self.pre_and_post_smoothing << l):
                    self.jacobi_smooth(l, self.z[l], self.z_new[l], res_cur)
                    self.z[bottom], self.z_new[bottom] = self.z_new[bottom], self.z[bottom]

            iter += 1
            residuals.append(np.max((self.z[0].to_numpy() - self.z_new[0].to_numpy())))
        return residuals

    @ti.kernel
    def reset(self):
        # TODO: deactivate all fields
        pass


# TODO: use double precision for reduction to save iterations?
