import cv2
import time
import mediapipe as mp
mp_drawing = mp.solutions.drawing_utils
mp_pose = mp.solutions.pose
# print(mp_pose.POSE_CONNECTIONS)

import threading
from utils import current_milli_time


class MediapipePoseEstimator(threading.Thread):

    def __init__(self, cam_reader):
        threading.Thread.__init__(self)
        self.cam_reader = cam_reader
        self.current_vis_frame = cam_reader.current_frame
        self.current_result = []
        self.current_timestamp = current_milli_time()
        
        self.predictor = mp_pose
        self.drawer = mp_drawing
        self.POSE_CONNECTIONS = mp_pose.POSE_CONNECTIONS
        self.pose = mp_pose.Pose(
            min_detection_confidence=0.5,
            min_tracking_confidence=0.5)

        self.last_processed_timestamp = None
        self.should_rest = False

    def predict(self, pose, image):
        return pose.process(image)

   
    def process_predictions(self,frame, predictions):
        # Draw the pose annotation on the frame.
        frame = self.cam_reader.current_frame
        frame.flags.writeable = True
        self.drawer.draw_landmarks(frame, predictions.pose_landmarks, self.POSE_CONNECTIONS)
        #cv2.imshow('Pose Estiamtion', pose_vis_frame)
        #if cv2.waitKey(5) & 0xFF ==27: pass
        vis_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        
        keypoints = []

        if predictions.pose_landmarks is not None:

            for data_point in predictions.pose_landmarks.landmark:
                keypoints.append({
                                    'x': data_point.x,
                                    'y': data_point.y,
                                    'z': data_point.z,
                                    'visibility': data_point.visibility,
                                    })
        
        return keypoints, vis_frame

    def run_on_video(self,run_detection):
        frame = self.cam_reader.current_frame
        self.last_processed_timestamp = self.cam_reader.current_timestamp
        if run_detection:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frame.flags.writeable = False
            keypoints, vis_frame = self.process_predictions(frame, self.predict(self.pose, frame))
            return keypoints, vis_frame
        else:
            return [], frame

    def run(self):
        while True:
            if self.last_processed_timestamp == self.cam_reader.current_timestamp:
                time.sleep(0.005)

            results, img = self.run_on_video(True)
            self.current_vis_frame = img
            self.current_result = results
            self.current_timestamp = current_milli_time()
